<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 01.02.17
 * Time: 1:05
 */

namespace app\commands;
use walmart_parser\WMParserComponent;
use Yii;
use yii\console\Controller;
use app\components\walmart_parser;
use Faker;


class ParseController extends Controller
{
    public function actionIndex()
    {
        /** @var $parser WMParserComponent*/
        $parser = new Yii::$app->WMParser;
        $parser->useIE = true;
        return $parser->getProducts('38455540');
    }
}
