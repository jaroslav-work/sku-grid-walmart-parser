<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 01.02.17
 * Time: 0:46
 */

namespace app\components\walmart_parser;
use Yii;
use app\components\walmart_parser\requests;
use GuzzleHttp\Cookie\SetCookie;
use yii\base\Component;
use yii\helpers\Json;
use yii\helpers\FileHelper;

class WMParserComponent extends Component
{
    public $useIE = false;
    public $primaryProductUrl = "https://www.walmart.com/ip/[:productID]";
    public $itemUrl = "https://www.walmart.com/terra-firma/item/[:MPN]?selected=true&wl13=";
    public $location_cookie_string = "DL=10001%2Cempty%2Cempty%2Cuser%2C10001%2CUSA%2CNY;Version=1;Domain=walmart.com;Path=/;";
    public $exhaustFolder;
    
    public function init()
    {
        $this->exhaustFolder = Yii::getAlias('@app') .
            DIRECTORY_SEPARATOR . 'runtime/exhaust';

        if (file_exists($this->exhaustFolder) || is_dir($this->exhaustFolder)) {
            FileHelper::removeDirectory($this->exhaustFolder);
            FileHelper::createDirectory($this->exhaustFolder);
        }
    }

    /**
     * @param integer $id Главное id товара.
     * @return array $products  Возвращает массив товаров "нашей" конструкции без лишних деталей.
     *  [
     *      'productName' => '...', //Mysql names conflict!
     *      'brand' => '...',
     *      'categoryPath' => '...',
     *      'shortDescription' => '...',
     *      'products' =>   [
     *                          [
     *                              'productID' => '0XR555129AXX', // какой-то адский идентификатор товара
     *                          'price' => 12.44,
     *                          'variantsSelection' => [ // Ключи вариантов, в зависимости от товара, могут меняться!
     *                              'actual_color' => 'Green',
     *                              'size' => 'Size 10.5',
     *                              ...
     *                          ],
     *
     *                          'status_stock' => 'IN_STOCK',
     *
     *                          'shippingOptions' => [
     *
     *                              [
     *                                  'name' => '',
     *                                  'price' => 42.3,
     *                              ],
     *                            ...
     *                          ],
     *                         ]
     *      ],
     *      ...
     *  ]
     *
     */
    public function getProducts($id)
    {
        $url = str_replace('[:productID]', $id, $this->primaryProductUrl);
        $location_cookie = SetCookie::fromString($this->location_cookie_string);
        
        $request = new requests\WMRequest($url);
        $request->cookies->setCookie($location_cookie);
        $request->setChildRequest(new requests\ProductRequest($this->itemUrl));
        $body = $request->send()->washBody();
        return $body;
    }


}