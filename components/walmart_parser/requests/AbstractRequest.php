<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 08.02.17
 * Time: 7:58
 */
namespace app\components\walmart_parser\requests;

use app\components\walmart_parser\context\Context;
use app\components\walmart_parser\prospectors\InfoProspector;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Faker;

class AbstractRequest implements RequestInterface
{
    public $url;
    public $params;

    public $resultBody;
    public $method = 'GET';
    public $userAgent;
    public $cookies;

    public function __construct($url)
    {
        $this->cookies = new CookieJar();
        $faker = Faker\Factory::create();
        $this->userAgent = $faker->userAgent;
        $this->url = $url;
    }

    public function send($method = null)
    {
        $client = new Client();
        if ($method === null) {
            $method = $this->method;
        }

        $response = $client->request($method, $this->url, [
            'headers' => [
                'User-Agent' => $this->userAgent,
            ]
        ]);
        $this->resultBody = $response->getBody();
        return $this;
    }

    public function washBody(InfoProspector $Prospector = null)
    {

        if ($Prospector === null) {
            $context = new Context($this);
            $this->resultBody = $context->execute();
        } else {
            $this->resultBody = $Prospector::wash($this->resultBody);
        }

        return $this->resultBody;
    }
}