<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 08.02.17
 * Time: 7:43
 */
namespace app\components\walmart_parser\requests;
interface RequestInterface {
    function __construct($url);
    function send();
    function washBody();
}
