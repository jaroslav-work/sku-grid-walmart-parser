<?php

/**
 * Created by PhpStorm.
 * User: inle
 * Date: 08.02.17
 * Time: 7:42
 */
namespace app\components\walmart_parser\requests;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Response;
use Faker;

class Factory
{
    private static $_pool;
    public $cookies;
    public $userAgent;
    
    public function __construct()
    {
        $faker = Faker\Factory::create();
        $this->cookies = new CookieJar();
        /*
         * При некоторых агентах не выполняется запрос.
         * Или выводятся другие скрипты на главной странице товара.
        */ 
        $this->userAgent = $faker->userAgent;
    }

    public static function Create(RequestInterface $request)
    {
        return self::$_pool[uniqid()] = $request;
    }

    public function send($method = 'GET', $washBodies = true)
    {
        $client = new Client();
        $promises = [];
        foreach (self::$_pool as $key => $request) {
            $promises[$key] = $client->requestAsync($method, $request->url, [
                'cookies' => $this->cookies,
                'headers' => [
                    'User-Agent' => $this->userAgent
                ],
            ]);
        }

        $results = Promise\settle($promises)->wait();

        foreach ($results as $key => $result) {
            if ($result['state'] === 'fulfilled') {
                $body = $result['value'];
                /**
                 * @var $body Response
                 * @var $elem AbstractRequest
                 */
                if ($body->getStatusCode() == 200) {
                    $elem = self::$_pool[$key];
                  
                    $elem->resultBody = $body->getBody();
                    self::$_pool[$key] = ($washBodies) ? $elem->washBody() :
                        $elem->resultBody = $elem->resultBody->getContents();
                }
            }
        }

        $pool = self::$_pool;
        self::$_pool = [];
        return $pool;
    }


}