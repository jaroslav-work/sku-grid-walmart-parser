<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 07.02.17
 * Time: 18:03
 */

namespace app\components\walmart_parser\requests;

use GuzzleHttp\Cookie\SetCookie;


class WMRequest extends AbstractRequest
{
    private $_ChildRequest = null; //Инстанс вторичного запроса
    public $childRequestParams = 'MPNs'; //Поле в котором хранятся параментры для вторичных запросов
    public $childStorageName = 'products'; //Ключи для массива-хранилища данных, вторичного запроса


    public function send($method = null)
    {
        $infoRequest = new InformationRequest($this->url);
        $information = $infoRequest->send()->washBody();

        if (!is_null($this->_ChildRequest)) {
            $Factory = new Factory();
            $information[$this->childStorageName] = [];
            $Factory->cookies = $this->cookies;

            $inPool = [];
            foreach ($information[$this->childRequestParams] as $MPN) {
                /** @var AbstractRequest $request */
                $request = $this->_ChildRequest;
                $request->url = str_replace('[:MPN]', $MPN, $request->url);
                $inPool[] = $Factory::Create($request);
            }
            $poolResults = $Factory->send();
            $information[$this->childStorageName] = array_values($poolResults);
        }

        $this->resultBody = $information;
        return $this;
    }

    /**
     * @param AbstractRequest|ProductRequest $Child
     * @internal param mixed $Secondary
     */
    public function setChildRequest(AbstractRequest $Child)
    {
        $this->_ChildRequest = $Child;
    }

}
