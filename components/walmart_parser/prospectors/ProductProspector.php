<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 02.02.17
 * Time: 12:45
 */

namespace app\components\walmart_parser\prospectors;

use Yii;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class ProductProspector extends BaseProspector
{
    public static $pathInstructions = [
        'offers' => "payload.offers",
        'products' => "payload.products",
        'selected' => "payload.selected",
        'variantSelection' => "payload.selected.variantSelection",
        'variantCategoriesMap' => 'payload.variantCategoriesMap',
        'primaryID' => 'payload.primaryProduct',
    ];

    /**
     * @param $rubbish
     * @return void|static    [
     *                          'productID' => '0XR555129AXX', // какой-то адский идентификатор товара
     *                          'price' => 12.44,
     *                          'variantsSelection' => [ // Ключи вариантов, в зависимости от товара, могут меняться!
     *                              'actual_color' => 'Green',
     *                              'size' => 'Size 10.5',
     *                              ...
     *                          ],
     *
     *                          'status_stock' => 'IN_STOCK',
     *
     *                          'shippingOptions' => [
     *
     *                              [
     *                                  'name' => '',
     *                                  'price' => 42.3,
     *                              ],
     *                            ...
     *                          ],
     *                       ]
     */
    public static function wash($rubbish)
    {
        /** @var $rubbish Stream */
        $body = $rubbish->getContents();
        $data = parent::wash(Json::decode($body));

        $product = self::findFetched($data['products']);
        $offer = self::findFetched($data['offers']);

        return [
            'productID' => $product['productId'],
            'price' => $offer['pricesInfo']['priceMap']['CURRENT']['price'],
            
            'variantsSelection' => self::normalizeVariants(
                                $data['variantCategoriesMap'], 
                                $data['variantSelection'],
                                $data['primaryID']
            ),
            
            'status_stock' => $offer['productAvailability']['availabilityStatus'],
            'shippingOptions' => self::getShippingOptions($offer)
        ];
    }

    protected static function normalizeVariants($variantsMap, $selection, $primaryProduct)
    {
        $result = [];
        $map = $variantsMap[$primaryProduct];
        foreach ($selection as $k => $v) {
            $result[$k] = $map[$k]['variants'][$v]['name'];
        }

        return $result;
    }


    protected static function getShippingOptions($offer)
    {
        $fulfillment = $offer['fulfillment'];
        $path_instructions = [
            'date_arrives' => 'fulfillmentDateRange.exactDeliveryDate',
            'date_earliest_arrives' => 'fulfillmentDateRange.earliestDeliverDate',
            'date_latest_arrives' => 'fulfillmentDateRange.latestDeliveryDate',
            
            'price_type' => 'fulfillmentPriceType',
            'method' => 'shipMethod',
            'price' => 'fulfillmentPrice.price',
        ];
        $options = [];
        if (isset($fulfillment['shippingOptions'])) {
            foreach ($fulfillment['shippingOptions'] as $option) {
                $new_option = [];
                foreach ($path_instructions as $k => $path_instruction) {
                    $new_option[$k] = self::getDataByPath($option, $path_instruction);
                }
                $options[] = $new_option;
            }
        }
        return $options;
    }

    /**
     * @param $items array Либо products либо offers
     * @return array вернёт выранный продукт/предложение
     */

    protected static function findFetched($items)
    {
        $fetched_items = [];
        foreach ($items as $index => $item) {
            if ($item['status'] == "FETCHED") {
                $fetched_items[] = $item;
            }
        }

        return $fetched_items[0];
    }

}