<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 02.02.17
 * Time: 12:57
 */
namespace app\components\walmart_parser\prospectors;


class BaseProspector extends base\AbstractDataProspector
{

    public static $pathInstructions = [];
    /**
     * @param $rubbish
     * @return self
     */
    public static function wash($rubbish)
    {
        $result = [];
        foreach (static::$pathInstructions as $key => $path) {
            $result[$key] = static::getDataByPath($rubbish, $path);
        }
        return $result;
    }

    /**
     * @param $rubbish
     * @param $path
     * @return mixed
     */
    public static function getDataByPath($rubbish, $path)
    {
        $path = explode('.', $path);
        $key_data = $rubbish;
        foreach ($path as $i => $key) {
           if (isset($key_data[$key])) {
               $key_data = $key_data[$key];
           } 
        }
        return $key_data;
    }


}