<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 03.02.17
 * Time: 7:38
 */
namespace app\components\walmart_parser\prospectors;

class ResultProspector extends BaseProspector {
    public static $path_instructions = [
        'categoryPath',
        'brand',
        'productName',
        'products',
    ];
    public $rubbish;
    
    public static function wash($rubbish)
    {
        $result = [];
        foreach (self::$path_instructions as $white_key) {
            $result[$white_key] = $rubbish[$white_key];
        }
        return $rubbish;
    }

}