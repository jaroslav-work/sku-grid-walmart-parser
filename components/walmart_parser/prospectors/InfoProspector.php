<?php

/**
 * Created by PhpStorm.
 * User: inle
 * Date: 02.02.17
 * Time: 0:53
 */
namespace app\components\walmart_parser\prospectors;


use app\components\walmart_parser\prospectors\base\ProspectorHelper;
use GuzzleHttp\Psr7\Stream;
use yii\helpers\ArrayHelper;
use yii\console\Exception;
use yii\helpers\Json;

class InfoProspector extends BaseProspector
{
    use ProspectorHelper;
    public static $pathInstructions = [
        'variantProducts' => 'variantInformation.variantProducts',
        'variantTypes' => 'variantInformation.variantTypes',
        'shortDescription' => 'shortDescription',
        'categoryPath' => 'analyticsData.catPath',
        'brand' => 'analyticsData.brand',
        'productName' => 'analyticsData.productName',
    ];

    /**
     * @param $rubbish
     * @return mixed|static
     */
    public static function wash($rubbish)
    {
        /**@var Stream $rubbish */
        $document = \phpQuery::newDocument($rubbish->getContents());
        $scripts = $document->find('script');
        $productInfo = [];
        foreach ($scripts as $k => $script) {
            if (strlen($script->textContent) > 1e4) {
                $json = self::parseJS4JSON($script->textContent);
                try {
                    $productInfo = Json::decode($json);
                } catch (Exception $e) {
                    print $e->getMessage();
                }
            }
        }

        $result = parent::wash($productInfo);
        $result['MPNs'] = array_keys(ArrayHelper::map($result['variantProducts'], 'id', 'fetched'));
        unset($result['variantProducts']);
        unset($result['variantTypes']);
        return $result;
    }


}