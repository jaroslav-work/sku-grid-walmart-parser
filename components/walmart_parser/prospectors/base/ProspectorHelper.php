<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 07.02.17
 * Time: 11:17
 */
namespace app\components\walmart_parser\prospectors\base;
trait ProspectorHelper {
    public static function parseJS4JSON($string) {
        /**
         *  /(define\("product\/data"\s?,\s?)(.*)(\s?\))/
         * Здесь рисую непонятную регулярку, которая почму-то не работает.
         * Работает только в online-редакторе. Всю эту ситацию можно обойти.
         */
        preg_match('/define\(\"product\/data",\s?(.*)\s?/', $string, $matches);
        return $matches[1];
    }
}