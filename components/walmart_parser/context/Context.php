<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 06.02.17
 * Time: 7:05
 */
namespace app\components\walmart_parser\context;

use app\components\walmart_parser\prospectors;
use app\components\walmart_parser\requests;
use yii\base\Exception;

class Context
{
    /**
     * Context constructor.
     * @param requests\RequestInterface $prospectorInterface
     * @internal param WMRequest $strategy
     */
    private $_Request;

    public function __construct(requests\AbstractRequest $request)
    {
        $this->_Request = $request;
    }

    /**
     * @return array|static
     * @throws Exception
     * @internal param $rubbish
     */
    public function execute()
    {
        switch (true) {
            case $this->_Request instanceof requests\InformationRequest :
                return prospectors\InfoProspector::wash($this->_Request->resultBody);
            case $this->_Request instanceof requests\ProductRequest :
                return prospectors\ProductProspector::wash($this->_Request->resultBody);
            case $this->_Request instanceof requests\WMRequest :
                return prospectors\ResultProspector::wash($this->_Request->resultBody);
            default:
                throw new Exception('Out of context!');
        }
    }
}